//"use strict";

//Express
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

//IO
const server = require('http').createServer(app);
const io = require('socket.io')(server);

server.listen(3333);

//ProcessManager
const process = require('./ProcessManager.js');
const processManager = new process();

io.on('connection', function(socket) {
	console.log('client connection');
});

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/index.html');
});

app.post('/process.run', function(req, res) {

	
	console.log(req.body.name);
	
	var success = processManager.run(req.body.name, req.body.params, function(a,b) {
        io.emit('processEnd', b);
	});

	if (success)
		res.send('sended');
	else
		res.send('command undefined');
	

});
